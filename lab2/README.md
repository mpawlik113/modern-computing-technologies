# Laboratorium 2

# Uwagi do pracy na klastrze DWARF


Praca z systemem kolejkowym PBS/Torque
--------------------------------------

### Sesja interaktywna

W celu utworzenia sesji inteaktywnej należy użyć skryptu session.sh
(dokonano poprawki w stosunku do poprzednich zajęć)

    session.sh <nodes> <ppn>

nodes - liczba używanych węzłów

ppn - liczba procesorów używanych na każdym węźle


### Wrzucanie zadań do kolejki

    qsub -q dteam job.sh

Istotne jest specyfikowanie kolejki do której wrzucane są zadania.


Testowanie oprogrmowania
------------------------

Wskazane jest testowanie każdej funkcji za pomocą testów jednostowych
(ang. unit tests). W tym laboratorium napisanie testów jednostkowych
jest zadaniem dodatkowym. Można do tego skorzystać z zewnętrznych
bibliotek, np. googletest.

Instrukcja instalacji biblioteki googletest

    git clone https://github.com/google/googletest.git
    cd googletest && mkdir build && cd build
    cmake -G"Unix Makefiles" ..
    make
    mkdir ~/libs/include && cp -r ../googletest/include/gtest ~/libs/include
    mkdir ~/libs/lib     && cp lib/*.a ~/libs/lib/

Wstęp do testów można znaleźć tutaj:
<https://github.com/google/googletest/blob/master/googletest/docs/primer.md>.

Macierz kowariancji
===================

Teoria
------

Niech będzie dane n zmiennych losowych z wielowymiarowego rozkładu
Gaussa i niech $\mu_{i}=\operatorname{E}\left[X_{i}\right]$ będzie
wartością oczekiwaną danej zmiennej. Macierzą kowariancji nazywamy:

```math
{\displaystyle \Sigma={\begin{bmatrix}\operatorname{E}[(X_{1}-\mu_{1})(X_{1}-\mu_{1})] & \operatorname{E}[(X_{1}-\mu_{1})(X_{2}-\mu_{2})] & \cdots & \operatorname{E}[(X_{1}-\mu_{1})(X_{n}-\mu_{n})]\\
\\
\operatorname{E}[(X_{2}-\mu_{2})(X_{1}-\mu_{1})] & \operatorname{E}[(X_{2}-\mu_{2})(X_{2}-\mu_{2})] & \cdots & \operatorname{E}[(X_{2}-\mu_{2})(X_{n}-\mu_{n})]\\
\\
\vdots & \vdots & \ddots & \vdots\\
\\
\operatorname{E}[(X_{n}-\mu_{n})(X_{1}-\mu_{1})] & \operatorname{E}[(X_{n}-\mu_{n})(X_{2}-\mu_{2})] & \cdots & \operatorname{E}[(X_{n}-\mu_{n})(X_{n}-\mu_{n})]
\end{bmatrix}}.}
```
```math
P\left(\mathbf{X}=\mathbf{x}\right)=\frac{1}{\left(2\pi\det\Sigma\right)^{n/2}}\exp\left[-\frac{1}{2}\mathbf{X}^{T}\Sigma\mathbf{X}\right]
```

Jeżeli wykonamy p pomiarów wektora
```math
\mathbf{X}=\left[X_{1}\,X_{2}\,\ldots\,X_{n}\right]
``` 
to otrzymamy macierz liczb $`\left\{ x_{ik}\right\} _{i\in\{1,\ldots,n\},\,k\in\{1,\ldots,p\}}`$ z których możemy obliczyć odpowiednie estymatory wartości oczekiwanych $`\mu_{i}`$ i elementów macierzy kowariancji.

### Zastosowania macierzy kowariancji

#### PCA (ang. Principal Components Analysis) lub Transformacja Karhunena-Loeve’a

Wektory własne macierzy kowariancji stanowią optymalną bazę do
przedstawienia zmiennej losowej $`\mathbf{X}`$.

```math
{\displaystyle V^{-1}\Sigma V=D}
```

D - macierz diagonalna, której elementy są wartościami własnymi

V - macierz wektorów własnych macierzy kowariancji.

Wykonanie transformacji

```math
y=V^{T}x
```

powoduje, że składowe wektora $`y`$ są niezależnymi zmiennymi losowymi,
jeżeli składowe wektora $`x`$ są wylosowane z wielowymiarowego rozkładu
Gaussa.

Dowolny proces stochastyczny można wyrazić jako rozwinięcie w szereg
ortonormalnych funkcji z losowymi współczynnikami (jeżeli są to zmienne
losowe niezależne), tak więc analogiczną transformację można zastosować
do szeregów czasowych.

Więcej informacji:
[pl.wikipedia.org/wiki/Analiza\_głównych\_składowych](https://pl.wikipedia.org/wiki/Analiza_g%C5%82%C3%B3wnych_sk%C5%82adowych)
i[en.wikipedia.org/wiki/Karhunen-Loeve\_theorem](https://en.wikipedia.org/wiki/Karhunen%E2%80%93Lo%C3%A8ve_theorem).

Obliczanie elementów macierzy kowariancji
-----------------------------------------

Pokazać, że elementy macierzy kowariancji można obliczyć wzorem:
```math
\Sigma_{ij}=\operatorname{E}[(X_{i}-\mu_{i})(X_{j}-\mu_{j})]=\operatorname{E}[X_{i}X_{j}]-\mu_{i}\mu_{j}=\frac{1}{p}\sum_{k=1}^{p}x_{ik}x_{jk}-\frac{1}{p^{2}}\left(\sum_{k=1}^{p}x_{ik}\right)\left(\sum_{k=1}^{p}x_{jk}\right)
```

#### Zadanie

Zaimplementować funkcję liczącą kowariancję pomiędzy dwoma wektorami
danych z wykorzystaniem MPI i OpenMP. Sprawdzić, że dla niezależnych
zmiennych losowych element macierzy kowariancji jest równy zeru (z
dokładnością numeryczną).

Program powinien być zaimplementowany tak by można było użyć n procesów
MPI i m wątków OpenMP (dla każdego procesu).

Wskazówka: przed przystąpieniem do zadania przeanalizować przykład
[mpitutorial.com/tutorials/mpi-reduce-and-allreduce/](http://mpitutorial.com/tutorials/mpi-reduce-and-allreduce/).

Challenge
=========

Zadanie
-------

**Należy napisać funkcję/bibliotekę wykonującą obliczenie całej macierzy
kowariancji. Na ostatnim laboratorium zostaną porównane kody wszystkich
studentów pod kątem szybkości wykonywania i skalowalności.**


#### Uwagi

-   Można zauważyć że wyrażenie $`C_{ij}=\sum_{k=1}^{p}x_{ik}x_{jk}=x^{T}x`$
    można wyrazić jako mnożenie macierzy i otrzymać macierz $n\times n$.

-   Standardowe operacje algebry liniowej są dostępne w bibliotekach
    BLAS i LAPACK.

-   Istnieją różne implementacje bibliotek BLAS i LAPACK, np. oba
    zestawy funkcji są dostarczane przez bibliotekę
    [OpenBLAS](https://github.com/xianyi/OpenBLAS).

-   Polecana jest również biblioteka
    [MAGMA](http://icl.cs.utk.edu/magma/index.html) używająca
    hybrydowych algorytmów, jednak jej skompilowanie wymaga
    wcześniejszego zainstalowania biblioteki dostarczająca BLAS w wersji
    na CPU (np. OpenBLAS) i GPU (np. cuBLAS dostępny na klastrze DWARF).

-   Funkcja
    [\*syrk](http://icl.cs.utk.edu/projectsfiles/magma/doxygen/group__magma__syrk.html)
    implementuje operację $`A=\alpha A+\beta BB^{T}`$

-   O funkcjach BLAS i LAPACK będzie mowa na kolejnych laboratoriach.

Sprawozdanie
============

Do sprawozdania z drugiego laboratorium należy zmierzyć czas wykonania
funkcji obliczającej element macierzy kowariancji dla różnej wartości
procesów MPI i ilości wątków OpenMP (dla procesów). Wyniki należy
przedstawić w formie wykresu i opatrzyć komentarzem.

Proszę zastanowić się jak prawidłowo mierzyć czas wykonania funkcji.
