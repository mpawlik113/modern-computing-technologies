#include <stdio.h>
#include <math.h>
#include <fftw3-mpi.h>


/*
 * 
 */
template <int nx, int ny=0, int nz=0>
void create_reciprocal_lattice(const double dx, double* kkx, double* kky = NULL, double* kkz = NULL)
{
    // =================== Initialize lattice in momentum space (first Brullion zone) ===================
    /* NOTE : nx , ny , nz = 2j forall j integers (e.g. even numbers for the lattice dimensions) */
    
    
    int i,j; 
    for ( i = 0 ; i <= nx/2-1; i++ ) { kkx[i] = 2. * (double) M_PI / nx / dx * (double) i; }
    j = - i ;
    for ( i = nx/2 ; i < nx; i++ )   { kkx[i] = 2. * (double) M_PI / nx / dx * (double) j; j++ ; }
    
    // create this only in 2d and 3d case
    if (kky != NULL)
    {
        for ( i = 0 ; i <= ny/2-1; i++ ) { kky[i] = 2. * (double) M_PI / ny / dx * (double) i; }
        j = - i ;
        for ( i = ny/2 ; i < ny; i++ )   { kky[i] = 2. * (double) M_PI / ny / dx * (double) j; j++;  }
    }
    
    // create this only in 3d case
    if (kkz != NULL)
    {
        for ( i = 0 ; i <= nz/2-1; i++ ) { kkz[i] = 2. * (double) M_PI / nz / dx * (double) i; }
        j = - i ;
        for ( i = nz/2 ; i < nz; i++ )   { kkz[i] = 2. * (double) M_PI / nz / dx * (double) j; j++;  }
    }
}


template <int nx, int ny=0, int nz=0>
void gaussian(fftw_complex* val, const double dx, const double sigma, const int ix, const int iy=0, const int iz=0)
{
    double x, y = 0, z = 0; 
    x = (ix - nx/2)*dx;
    if (ny > 0) y = (iy - ny/2)*dx;
    if (nz > 0) z = (iz - nz/2)*dx;
    *val[0] =  exp( -(x*x + y*y + z*z)/(2.0*sigma*sigma) );
    *val[1] = 0;
}


/*
 * source: http://www.fftw.org/doc/2d-MPI-example.html
 * mpic++ -std=c++11 -o fftw3_2d_example.x fftw3_2d_example.cpp -I/usr/local/include -L/usr/local/lib -lfftw3_mpi -lfftw3
 * usage: mpirun -n <> fftw3_2d_example.x > fftw3_2d_example.txt
 */
int main(int argc, char **argv)
{
    const ptrdiff_t N0 = 256, N1 = 256;
    const double L  = 20.0;
    const double dx = L/((double) N0);
    const double sigma = 1.0;
    fftw_plan plan;
    fftw_complex *data;
    ptrdiff_t alloc_local, local_n0, local_0_start;

    MPI_Init(&argc, &argv);
    fftw_mpi_init();
    
    
    // first get id of each process and total number of processes
    int ip, np;
    MPI_Comm_rank (MPI_COMM_WORLD, &ip);
    MPI_Comm_size (MPI_COMM_WORLD, &np);
    
    
    /* using MPI_COMM_WORLD means that all mpi processes are involved in computation of fft */
    /* to change this one needs to create another mpi communicator */
    /* get local data size and allocate */
    alloc_local = fftw_mpi_local_size_2d(N0, N1, MPI_COMM_WORLD, &local_n0, &local_0_start);
    data = fftw_alloc_complex(alloc_local);

    /* create plan for in-place forward DFT */
    plan = fftw_mpi_plan_dft_2d(N0, N1, data, data, MPI_COMM_WORLD,
                                FFTW_FORWARD, FFTW_ESTIMATE);    
    
    /* initialize data to some function my_function(x,y) */
    for (int ix = 0; ix < local_n0; ++ix)
    for (int iy = 0; iy < N1; ++iy)
    {
       int ixy = ix*N1 + iy;
       gaussian<N0, N1>( &(data[ixy]), dx, sigma, local_0_start + ix, iy);
    }
    
    /* compute transforms, in-place, as many times as desired */
    fftw_execute(plan);
    
    /* allocate reciprocal lattice - for each process same, can be done better */
    double* kkx = (double*) malloc( N0*sizeof(double) );
    double* kky = (double*) malloc( N1*sizeof(double) );
    create_reciprocal_lattice<N0, N1>(dx, kkx, kky);
    
    
    // print data on screen
    for (int i=0; i<np; i++) // iterate over all processes
    {
        if (i==ip) // if process match iteration of loop
        {
            for (int ix = 0; ix < local_n0; ++ix) // iterate over data stored by current process
            for (int iy = 0; iy < N1; ++iy)
            {
                int ixy = ix*N1 + iy;
                printf("%4d \t %4d \t %8d \t %10.5lf \t %10.5lf\t %10.5lf\t %10.5lf \n",
                       ixy,ix+(int)local_0_start,iy,kkx[ix+local_0_start],kky[iy],data[ixy][0],data[ixy][1]);
            }
        }
        /* wait until ip-th process has printed all data */
        MPI_Barrier(MPI_COMM_WORLD);
    }
    
    fftw_destroy_plan(plan);
    free(kkx);
    free(kky);
    
    MPI_Finalize();
}
