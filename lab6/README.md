Macierze rzadkie
================

![](sparse-matrix.jpg)
<sup>Źródło: http://btechsmartclass.com/DS/images/U1_T14_P1.png</sup>

 

Tym określeniem nazywa się macierze, których większość elementów jest
zerowa.

Liczbę niezerowych elementów będziemy oznaczać przez nnz.

“Gęstość” macierzy można oznaczyć jako stosunek liczby niezerowych
elementów do wszystkich elementów $`\frac{\text{nnz}}{\text{N}\times\text{N}}`$ (gdzie N to wymiar macierzy).


Formaty macierzy rzadkich
-------------------------

Opis najczęściej używanych formatów jest podany w dokumentacji biblioteki cusparse:
<https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-indexing-and-data-formats>.


### Format COO

Jest to najprostszy do zastosowania format macierzy rzadkiej.

Macierz jest reprezentowana przez trzy tablice, każda o długości nnz. Jedna zawiera niezerowe wartości, a pozostałe zawierają indeksy wiersza i kolumny elementu.

### Formaty CSR i CSC

Macierz jest reprezentowana przez trzy tablice, dwie o długości nnz i jedną o długości n+1, gdzie n to wymiar macierzy (kwadratowej).

* Co znajduje się w tych tablicach?

* Jaki związek ze sobą mają oba formaty?


Konwersja formatów macierzy rzadkiej w bibliotece cusparse
----------------------------------------------------------

Biblioteka cusparse zawiera wiele przydatnych funkcji do konwersji formatów: <https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-format-conversion-reference>.

Czasami jest łatwiej wygenerować macierz w jednym formacie, ale do zastosowania konkrentnego algorytmu wydajnieszy jest inny.

Przy konwersji należy pamiętać w jakiej kolejności są zapisane wartości elementów w danej macierzy.


Zastosowania
------------

* Rozwiązywanie równań różniczkowych w hydrodynamice, mechanice ośrodków
ciągłych - dowolny operator różniczkowy można zawsze przybliżyć w postaci macierzy i wielu
przydatnych reprezentacjach jest to macierz rzadka. Rozważmy choćby dwu-
lub trzy- wymiarowy laplasjan w reprezentacji na siatce.

* Analiza danych i uczenie maszynowe.

* Macierz sąsiedztwa grafu.



Grafy
=====

$`  A=\left[{\begin{matrix}0&1&0&0&1&0\\1&0&1&0&1&0\\0&1&0&1&0&0\\0&0&1&0&1&1\\1&1&0&1&0&0\\0&0&0&1&0&0\end{matrix}}\right] `$

![](333px-6n-graf.png)

Reprezentacja grafu w postaci macierzy sąsiedztwa
-------------------------------------------------

Do wielkoskalowych obliczeń chcielibyśmy zamienić operacje na grafach na
operacje algebry liniowej.

Macierz sąsiedztwa to macierz $`n\times n`$ o wartościach 0,1, ij-ty
element macierzy mówi o tym czy istnieje połączenie pomiędzy i-tym a
j-tym wierzchołkiem grafu.

Dla grafu nieskierowanego musi to być macierz symetryczna, tzn. jeżeli
istnieje połączenie wierzchołka i-tego z j-tym to istnieje połączenie
wierzchołka j-tego z i-tym.

Proszę przejrzeć hasła na wikipedii: <https://en.wikipedia.org/wiki/Adjacency_matrix> i <https://pl.wikipedia.org/wiki/Macierz_s%C4%85siedztwa>.


Grafy losowe (Erdosa-Renyi'ego)
--------------------------------------

Niech $`p`$ oznacza prawdopodobieństwo że istniej krawędź pomiędzy dwoma
dowolnymi wierzchołkami i,j.

Niech $`n`$ oznacza liczbę wierzchołków

Rozkład stopnia wierzchołka: $` P(\deg(v)=k)={n-1 \choose k}p^{k}(1-p)^{n-1-k}. `$

<https://en.wikipedia.org/wiki/Erd%C5%91s%E2%80%93R%C3%A9nyi_model>.


Biblioteka nvgraph
-------------------

Typowe algorytmy w grafach są zaimplementowane w bibliotece nvgraph: <https://docs.nvidia.com/cuda/nvgraph/index.html>.

W jaki sposób przekonwertować macierz sąsiedztwa na 'topologię' grafu w bibliotece nvgraph?



Ciekawe zastosowania macierzy sąsiedztwa (dodatkowo)
=========================================

Liczba trojkątów w grafie
-------------------------

Dla grafów E-R można uzyskać wynik analityczny $`N_{t}={{n \choose 3}}p^{3}`$.

Można skorzystać z właściwości macierzy sąsiedztwa
$`N_{t}=\frac{1}{3!}\text{tr}A^{3}`$
<https://en.wikipedia.org/wiki/Adjacency_matrix#Matrix_powers>.


Biblioteka nvgraph dostarcza także gotową funkcję do liczenia trojkątów w grafie: <https://docs.nvidia.com/cuda/nvgraph/index.html#function-nvgraphtricount>. Przykład jej użycia jest w pliku nvgraph_triangles.cu.


Podnoszenie do potęgi macierzy 
------------------------------

Należy do tego użyć funkcji: <https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-lt-t-gt-csrgemm> lub <https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-lt-t-gt-csrgemm2>.

Jest to jedyna funkcja wspierająca mnożenie dwóch rzadkich macierzy, ale istnieją także funkcje mnożenia macierzy rzadkiej i gęstej także są dostępne.

Należy zwrócić uwagę, że macierz wynikowa ma inną ilość niezerowych elementów i nie jest ona znana a proiri.

Komentarz: sam przy tym poległem, ale przykład dostarczany przez NVIDIĘ jest w pliku sparce_gemm.cu


Podział spektralny grafu 
------------------------

Istnieje relatywnie prosty algorytm dzielenia grafu tak by zminimalizować liczbę przecinanych krawędzi w czasie podziału.

Najpierw należy stworzyć laplasjan grafu

$`L=D-A`$

Gdzie macierz A to macierz sąsiedztwa, a macierz D to diagonalna macierz zawierająca stopnie wierzchołków na
przekątnej.

Do dodawania macierzy rzadkich można użyć funkcji:
<https://docs.nvidia.com/cuda/cusparse/index.html#cusparse-lt-t-gt-csrgeam>.

Ewentualnie można napisać własną funkcję (kernel) obliczającą laplasjan.

Wektory własne laplasjanu (przybliżamy tak by te wektory zawierały tylko 0 i 1) wyznaczają optymalny podział (jeżeli i-ty element wektora jest równy 0 to nie należy on do tej grupy wierzchołków). Optymalną ilość grup na które dzielimy można otrzymać analizaując wartości własne.

Dokładniejszy opis można znaleźć tutaj:
<https://devblogs.nvidia.com/fast-spectral-graph-partitioning-gpus/>.



Wykonanie zadania i sprawozdanie
================================


Wygenerowanie symetrycznej macierzy 
------------------------------------

Przykład jak wygenrować symetryczną macierz w formacie COO można znaleźć
w pliku cusparse\_gemm.cu.

* Jaki dodatkowy warunek musi spełniać macierz by być macierzą sąsiedztwa?

* Jaki związek ma p z gęstością macierzy?


Obliczanie stopni wierzchołków
-------------------------------

* Napisać kernel liczący stopień wierzchołka grafu reprezentowanego przez macierz rzadką. Można zastosować format COO lub CSR.

* Następnie należy sporządzić wykres przedstawiający rozkład stopnia wierzchołka w zależności
od $`p`$ dla kilku różnych $`n`$. Należy pamiętać, że liczba wierzchołków
grafu musi być odpowiednio duża by jego statystyczne własności były
zachowane.

* Zmierzyć czas wykonywania się algorytmu w zależności od $`p`$ i $`n`$.

* Kiedy opłacalne jest stosowanie macierzy rzadkich?

Reprezentacja macierzy w pamięci komputera
------------------------------------------

* Jaką ilość pamięci trzeba zaalokwać by trzymać macierze gęste w formacie COO i CSR? Sporządzić odpowiednie wykresy ilości pamięci zajmowanej przez macierz w danym formacie w zależności od n i gęstości macierzy. Porównać z typową reprezentacją macierzy jako bloku pamięci $`n \times n`$.



