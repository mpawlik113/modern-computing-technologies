General matrix-vector (GEMV) i General matrix-matrix (GEMM) 
===========================================================

Jesteśmy zainteresowani wykonywaniem operacji mnożenia dwóch macierzy o
dowolnym wymiarze, przy czym należy pamiętać, że wektor to macierz,
której jeden z wymiarów jest równy 1.


Operacje mnożenia macierzy są zaimplementowane m.in. w bibliotece BLAS,
przy czym istnieją różne wersje tej bliblioteki. Warte wzmionki są MKL,
OpenBLAS, cuBLAS, MAGMA. Wszystkie te biblioteki działają w ramach
jednego węzła obliczeniowego, natomiast problematyczne może być
znalezienie gotowego rozwiązania działającego na kilku węzłach.

Przykład mnożenia macierzy na jednym węźle na CPU z użyciem MKL:
<https://software.intel.com/en-us/mkl-tutorial-c-multiplying-matrices-using-dgemm>.

Zauważmy, że jeżeli podzielimy dowolną macierz kwadratową $`A`$ na NxN
części, a wektor $`\mathbf{b}`$ o długości równej bokowi macierzy to
mnożenie $`A\mathbf{b}`$ można przedstawić jako

$`A\mathbf{b}=\sum_{j=1}^{N}A_{ij}\mathbf{b}_{i}`$

(Porównaj oznaczenia z rysunkiem obok)

![](GEMV_grid.png)

Przykładowe zastosowania
------------------------

### Rozwiązywanie układów równań liniowych

W przypadku układu równań typu $`A\mathbf{x}=\mathbf{b}`$, gdzie
$`A,\mathbf{b}`$ są znane oraz $`A`$ jest macierzą przekątniowo
dominującą, znany jest prosty algorytm oparty o mnożenie macierzy i
wektorów: <https://en.wikipedia.org/wiki/Jacobi_method>.

Zauważmy, że jeżeli chcemy rozwiązać wiele równań liniowych z takimi
samymi macierzami $`A`$ a różnymi wektorami $`\mathbf{b}`$ to dalej możemy
stosować taki sam algorytm, tylko $`\mathbf{b},\mathbf{x}`$ należy
zastąpić przez macierze, których kolumny są kolejnymi wektorami.

Przykładowo rozważmy równanie Poissona
$`\nabla^{2}\varphi=\varepsilon_{0}\rho`$ i różne rozkłady ładunku po
prawej stronie równania.

### Sieci neuronowe

Przystępne wprowadzenie do wykorzystania mnożenia macierzy w tzw. deep
learningu można znaleźć tu
[petewarden.com/](https://petewarden.com/2015/04/20/why-gemm-is-at-the-heart-of-deep-learning/).

Zadanie
=======

Zaimplementować bibliotekę[^1] wykonującą mnożenie macierz-wektor z
użyciem MPI i GPU. Należy podzielić całość zadania pomiędzy poszczególne
procesy MPI. Sposób podziału opisać i uzasadnić w sprawozdaniu.

Należy przemyśleć użycie następujących funkcji:

-   [cublas
    gemv](https://docs.nvidia.com/cuda/cublas/#cublas-lt-t-gt-gemv)

-   [cublas
    gemm](https://docs.nvidia.com/cuda/cublas/#cublas-lt-t-gt-gemm)

-   [cublas gemm
    batched](https://docs.nvidia.com/cuda/cublas/#cublas-lt-t-gt-gemmbatched) -
    czy jest uzasadnienie do użycia tej funkcji w tym przypadku?

-   [cublasXt
    gemm](https://docs.nvidia.com/cuda/cublas/#cublasxt_gemm) - wersja
    używająca wielu GPU w ramach jednego węzła.

Wybór funkcji uzasadnić w sprawozdaniu.

W przypadku wyboru “zwykłych” funkcji cuBLAS można przyjąć że każdy GPU
ma odpowiadający mu process MPI.

W dla funkcji cuBLASXt należy stworzyć procesy dla każdej grupy GPU, np.
jeden proces MPI dla każdego węzła.

Należy oszacować efektywność wykorzystania mocy obliczeniowej i opisać w
sprawozdaniu.

Jak rozwiązać komunikację pomiędzy procesami MPI?

Skonstruowaną bibliotekę należy wykorzystać do rozwiązania wybranego
układu równań liniowych. Należy wybrać problem, który nie mieści się w
pamięci RAM jednego węzła. Opisać to w sprawozdaniu.

W przypadku braku natchnienia można użyć generatora: [https://math.nist.gov/MatrixMarket/data/misc/xlatmr/matgen.html](https://math.nist.gov/MatrixMarket/data/misc/xlatmr/matgen.html).
Macierze z plików można wczytać używając MPI IO, przykłady tu
[http://wgropp.cs.illinois.edu/courses/cs598-s16/](http://wgropp.cs.illinois.edu/courses/cs598-s16/lectures/lecture32.pdf).
Po wykonaniu pracy proszę usunąć dane z dysku (mogą zajmować za dużo miejsca).

[^1]: Tzn. może to być funkcja lub klasa, chodzi o to by można było
    łatwo włączyć kod do różnych programów.

## Przykład podziału komunikatorów w MPI

Domyślnym komunikatorem MPI jest MPI_COMM_WORLD (jest to struktura wbudowana w bibliotekę MPI).

Możemy tworzyć swoje własne komunikatory, np. komunikator procesów wykonujących się na jednym węźle. Kod który to robi jest poniżej:

```
int shmrank;
MPI_Comm shmcomm;
MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &shmcomm);
MPI_Comm_rank(shmcomm, &shmrank);
```

Teraz zmienna `shmrank` przechowuje numer procesu na danym węźle.


Więcej przykładów: [http://mpitutorial.com/tutorials/introduction-to-groups-and-communicators/](http://mpitutorial.com/tutorials/introduction-to-groups-and-communicators/)
