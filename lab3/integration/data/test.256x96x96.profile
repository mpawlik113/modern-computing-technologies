==4775== NVPROF is profiling process 4775, command: ./expected_val.exe
==4775== Profiling application: ./expected_val.exe
==4775== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.159432,67.781085,100,0.677810,0.671985,0.697808,"void kernel_RC_mult<int=256, int=96, int=96, int=1>(double const *, double2 const *, double2*)"
20.672489,55.692981,100,0.556929,0.535379,0.769102,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.555997,52.685081,100,0.526850,0.518580,0.537652,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.185458,46.298700,100,0.462987,0.450902,0.475125,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.610827,39.362482,100,0.393624,0.391127,0.407767,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.677582,4.519512,3,1.506504,0.000992,3.021978,"[CUDA memcpy HtoD]"
0.320616,0.863760,100,0.008637,0.008447,0.009440,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.230390,0.620684,102,0.006085,0.003392,0.268441,"[CUDA memset]"
0.220139,0.593069,400,0.001482,0.001312,0.008352,"[CUDA memcpy DtoH]"
0.138756,0.373817,100,0.003738,0.003616,0.004672,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.135205,0.364250,100,0.003642,0.003488,0.003872,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.093109,0.250842,100,0.002508,0.002464,0.002624,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4775== API calls:
No API activities were profiled.
