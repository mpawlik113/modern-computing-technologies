==3742== NVPROF is profiling process 3742, command: ./expected_val.exe
==3742== Profiling application: ./expected_val.exe
==3742== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
20.720535,7.184815,100,0.071848,0.067358,0.077054,"void kernel_RC_mult<int=64, int=64, int=64, int=1>(double const *, double2 const *, double2*)"
19.215081,6.662801,100,0.066628,0.065598,0.071390,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
19.048456,6.605024,100,0.066050,0.064478,0.070046,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
17.581073,6.096211,100,0.060962,0.059615,0.068447,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
14.542793,5.042692,100,0.050426,0.049246,0.054623,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
2.577571,0.893769,100,0.008937,0.008735,0.009215,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
1.693138,0.587093,400,0.001467,0.001376,0.001792,"[CUDA memcpy DtoH]"
1.479582,0.513043,3,0.171014,0.000992,0.345368,"[CUDA memcpy HtoD]"
1.249496,0.433261,100,0.004332,0.004224,0.004512,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
1.099004,0.381078,102,0.003736,0.003392,0.032575,"[CUDA memset]"
0.793269,0.275065,100,0.002750,0.002688,0.002975,"void __reduce_kernel__<unsigned int=128, double>(double*, double*, int)"

==3742== API calls:
No API activities were profiled.
