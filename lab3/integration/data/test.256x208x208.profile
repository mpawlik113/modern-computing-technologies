==5303== NVPROF is profiling process 5303, command: ./expected_val.exe
==5303== Profiling application: ./expected_val.exe
==5303== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
26.062229,322.904157,100,3.229041,3.179767,3.626125,"void kernel_RC_mult<int=256, int=208, int=208, int=1>(double const *, double2 const *, double2*)"
20.395761,252.698107,100,2.526981,2.507367,2.871230,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.853410,245.978522,100,2.459785,2.438984,2.503558,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.761941,207.676038,100,2.076760,2.055569,2.110224,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.873113,184.273957,100,1.842739,1.813783,2.061809,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.722690,21.343670,3,7.114556,0.001248,14.244217,"[CUDA memcpy HtoD]"
0.134356,1.664635,102,0.016319,0.003040,1.335521,"[CUDA memset]"
0.068339,0.846700,100,0.008467,0.008288,0.009344,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.045885,0.568499,400,0.001421,0.001312,0.001824,"[CUDA memcpy DtoH]"
0.034324,0.425270,100,0.004252,0.004032,0.004416,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.029973,0.371354,100,0.003713,0.003456,0.004128,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.017980,0.222770,100,0.002227,0.002175,0.002400,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5303== API calls:
No API activities were profiled.
