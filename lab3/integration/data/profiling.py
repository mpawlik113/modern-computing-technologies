from __future__ import print_function,division

import math
import numpy as np
import scipy.special
import matplotlib.pyplot as plt
import matplotlib.cm as cm

plt.style.use('ggplot')

import re
import time
import sys
import glob
import csv 

#Function to add ticks
def addticks(ax,newLocs,newLabels,pos='x',replace=True, rotation='vertical',fontsize=10):
    # Draw to get ticks
    plt.draw()
    
    # Get existing ticks
    if pos=='x':
        locs   = ax.get_xticks().tolist()
        labels = [x.get_text() for x in ax.get_xticklabels()]
    elif pos =='y':
        locs = ax.get_yticks().tolist()
        labels=[x.get_text() for x in ax.get_yticklabels()]
    else:
        print("WRONG pos. Use 'x' or 'y'")
        return
    
    if replace is True:
        Dticks=dict(zip(newLocs,newLabels))
    else:
        # Build dictionary of ticks
        Dticks=dict(zip(locs,labels))
        
        # Add/Replace new ticks
        for Loc,Lab in zip(newLocs,newLabels):
            Dticks[Loc]=Lab

    # Get back tick lists
    locs=list(Dticks.keys())
    labels=list(Dticks.values())

    # Generate new ticks
    if pos=='x':
        ax.set_xticks(locs)
        ax.set_xticklabels(labels,rotation=rotation,fontsize=fontsize)
    elif pos =='y':
        ax.set_yticks(locs)
        ax.set_yticklabels(labels,rotation=rotation,fontsize=fontsize)


numeric_const_pattern = r"""
     [-+]? # optional sign
     (?:
         (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
         |
         (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
     )
     # followed by optional exponent part if desired
     (?: [Ee] [+-]? \d+ ) ?
     """
rx = re.compile(numeric_const_pattern, re.VERBOSE)

def proces_csv(reader):
    data = []
    it = 1
    ifft = 1
    for row in reader:
        # check if no errors in profiling/execution
        if (len(row) > 0) and row[0].find('Error') is not -1:
            #print(row,'\n\n\n')
            return None
        # find timings for kenels and append to data
        if (len(row) > 6) and (len(rx.findall(row[0])) > 0) and (len(row[2]) > 1):
            if  (row[6].find('thrust') is not -1):
                key = 'thrust{:d}'.format(it)
                it   += 1
            elif (row[6].find('radix') is not -1) or (row[6].find('__nv_static') is not -1) or (row[6].find('fft') is not -1):
                key = 'cufft{:d}'.format(ifft)
                ifft += 1
            else:
                key = row[6]
            data.append( (key,[float(x) for x in row[:6]]) )
    return dict(data)

def plot_timings(prefix='coulomb',only_pow2=False):
    files = sorted(glob.glob(prefix+'.*.profile'))
    sizes = np.array([ [float(x) for x in re.findall(r'\d+',f)] for f in files ])
    
    print(sizes)
    
    sizes_tot, files = (list(t) for t in zip(*sorted(zip(sizes[:,0]*sizes[:,1]*sizes[:,2], files))))
    sizes = np.array([ [float(x) for x in re.findall(r'\d+',f)] for f in files ])
    
    sizes_tot = np.array(sizes_tot)
    files = np.array(files)
    print('number of files:',len(files))
    print(files)
    print(sizes)
    print(sizes_tot)
    
    keys = ['[CUDA memset]',
            'thrust1',
            'thrust2',
            'void kernel_compare_multipass<double, unsigned int=512, bool=1>(double const *, double*, unsigned int, int*, double)',
            'void kernel_compare<double>(double*, double*, int*, double, int)',
            '[CUDA memcpy DtoH]']
    
    
    avg_t_thrust   = np.zeros(len(files))
    min_t_thrust   = np.zeros(len(files))
    max_t_thrust   = np.zeros(len(files))
    avg_t_custom = np.zeros(len(files))
    min_t_custom = np.zeros(len(files))
    max_t_custom = np.zeros(len(files))
    avg_t_cublas = np.zeros(len(files))
    min_t_cublas = np.zeros(len(files))
    max_t_cublas = np.zeros(len(files))
    avg_t_cublas2 = np.zeros(len(files))
    min_t_cublas2 = np.zeros(len(files))
    max_t_cublas2 = np.zeros(len(files))
    
    for it,f in enumerate(files):
        print(f)
        with open(f,'rb') as csvfile: 
            reader = csv.reader(csvfile)
            
            data = proces_csv(reader)
            if data is None:
                avg_t_thrust[it] = np.nan
                min_t_thrust[it] = np.nan
                max_t_thrust[it] = np.nan
                continue
            
            for key in data:
                print(key,data[key])
                if key.find('thrust') is not -1:
                    avg_t_thrust[it] += data[key][1]#*data[key][2]
                    min_t_thrust[it] += data[key][4]
                    max_t_thrust[it] += data[key][5]
                elif key.find('kernel_DZdreduce<') is not -1:
                    avg_t_custom[it] += data[key][1]#*data[key][2]
                    min_t_custom[it] += data[key][4]
                    max_t_custom[it] += data[key][5]
                elif key.find('__reduce_kernel__') is not -1:
                    avg_t_custom[it] += data[key][1]#*data[key][2]
                    min_t_custom[it] += data[key][4]
                    max_t_custom[it] += data[key][5]
                elif key.find('dot_kernel') is not -1:
                    avg_t_cublas[it] += data[key][1]#*data[key][2]
                    min_t_cublas[it] += data[key][4]
                    max_t_cublas[it] += data[key][5]
                elif key.find('reduce_1Block_kernel') is not -1:
                    avg_t_cublas[it] += data[key][1]#*data[key][2]
                    min_t_cublas[it] += data[key][4]
                    max_t_cublas[it] += data[key][5]
                elif key.find('kernel_RC_mult') is not -1:
                    avg_t_cublas2[it] += data[key][1]#*data[key][2]
                    min_t_cublas2[it] += data[key][4]
                    max_t_cublas2[it] += data[key][5]
            csvfile.close()
        #print('\n\n\n')
    print('data read')
    
    # get rid of nans due to errors
    indexes = np.logical_not(np.isnan(avg_t_thrust))
    if (only_pow2):
        indexes = np.logical_and( indexes, sizes_tot%65536 == 0 )
    sizes_tot         =         sizes_tot[indexes]
    files = files[indexes]
    avg_t_thrust = avg_t_thrust[indexes]
    min_t_thrust = min_t_thrust[indexes]
    max_t_thrust = max_t_thrust[indexes]
    avg_t_custom = avg_t_custom[indexes]
    min_t_custom = min_t_custom[indexes]
    max_t_custom = max_t_custom[indexes]
    avg_t_cublas = avg_t_cublas[indexes]
    min_t_cublas = min_t_cublas[indexes]
    max_t_cublas = max_t_cublas[indexes]
    avg_t_cublas2 = avg_t_cublas2[indexes]
    min_t_cublas2 = min_t_cublas2[indexes]
    max_t_cublas2 = max_t_cublas2[indexes]
    
    #sizes_tot = np.log(sizes_tot)/np.log(2)
    
    #plt.figure(figsize=[18.,12.])
    fig,ax = plt.subplots(1, 1,figsize=[18.,12.])
    plt.suptitle('GeForce GTX 980M, architecture Maxwell, compute capability 5.2')
    ax.set_yscale('log')
    ax.set_ylabel('[ms]')
    #ax.errorbar(sizes_tot, avg_t_thrust, yerr=[min_t_thrust,max_t_thrust],marker='o',markersize=1.5,label='cuFFT time')
    bar_pos = 8*np.arange(len(sizes_tot),dtype=np.float32)
    width = 1
    #ax.set_xlim([-*width,3*avg_t_thrust.size+3*width])
    ax.bar(bar_pos-2*width, avg_t_thrust  , width,color='blue',label='thrust')#, yerr=max_t_cufft)
    ax.bar(bar_pos-1*width, avg_t_custom, width,color='green',label='custom')#, yerr=max_t_cufft)
    ax.bar(bar_pos+0*width, avg_t_cublas, width,color='orange',label='cublas')#, yerr=max_t_cufft)
    ax.bar(bar_pos+1*width, avg_t_cublas2, width,color='red',label='mult (before cublas)')#, yerr=max_t_cufft)
    ax.legend(loc='upper left')
    addticks(ax,bar_pos,['' for f in files],pos='x',fontsize=9)
    
    ax.set_xlim([np.min(bar_pos)-width,np.max(bar_pos)+width])
    addticks(ax,bar_pos,[f.split('.')[1] for f in files],pos='x',fontsize=9)
    
    
    plt.savefig('timings_exp_val.pdf')



if __name__ == '__main__':
    plot_timings('test')