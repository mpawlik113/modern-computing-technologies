==4188== NVPROF is profiling process 4188, command: ./expected_val.exe
==4188== Profiling application: ./expected_val.exe
==4188== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
23.291308,14.815735,100,0.148157,0.145437,0.161692,"void kernel_RC_mult<int=128, int=64, int=64, int=1>(double const *, double2 const *, double2*)"
19.573437,12.450776,100,0.124507,0.122077,0.128957,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
18.571956,11.813728,100,0.118137,0.116285,0.121022,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
18.247352,11.607246,100,0.116072,0.111933,0.122589,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.709706,9.356929,100,0.093569,0.091262,0.095646,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.592300,1.012871,3,0.337623,0.000960,0.678095,"[CUDA memcpy HtoD]"
1.396563,0.888362,100,0.008883,0.008703,0.009152,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.915554,0.582389,400,0.001455,0.001343,0.007360,"[CUDA memcpy DtoH]"
0.625849,0.398106,100,0.003981,0.003904,0.004160,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.605267,0.385014,102,0.003774,0.003167,0.060127,"[CUDA memset]"
0.470708,0.299420,100,0.002994,0.002911,0.003200,"void __reduce_kernel__<unsigned int=256, double>(double*, double*, int)"

==4188== API calls:
No API activities were profiled.
