==5525== NVPROF is profiling process 5525, command: ./expected_val.exe
==5525== Profiling application: ./expected_val.exe
==5525== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
26.090652,488.226636,100,4.882266,4.821490,5.372806,"void kernel_RC_mult<int=256, int=256, int=256, int=1>(double const *, double2 const *, double2*)"
20.573153,384.979316,100,3.849793,3.791146,4.190337,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=1>(double const *, double2*, double*, unsigned int)"
19.881913,372.044351,100,3.720443,3.690188,3.766123,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.707150,312.635942,100,3.126359,3.087354,3.161944,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.773161,276.445788,100,2.764457,2.744386,3.167096,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.715560,32.102759,3,10.700919,0.001280,21.400954,"[CUDA memcpy HtoD]"
0.123043,2.302476,102,0.022573,0.003423,1.950868,"[CUDA memset]"
0.045335,0.848336,100,0.008483,0.008224,0.009312,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.030189,0.564912,400,0.001412,0.001312,0.001696,"[CUDA memcpy DtoH]"
0.023954,0.448238,100,0.004482,0.004320,0.004703,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.022745,0.425623,100,0.004256,0.003904,0.004800,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.013147,0.246008,100,0.002460,0.002399,0.002688,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5525== API calls:
No API activities were profiled.
