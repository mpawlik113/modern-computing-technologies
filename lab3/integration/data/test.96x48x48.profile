==3820== NVPROF is profiling process 3820, command: ./expected_val.exe
==3820== Profiling application: ./expected_val.exe
==3820== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
20.425415,6.253353,100,0.062533,0.058335,0.067167,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
19.428291,5.948078,100,0.059480,0.056127,0.066270,"void kernel_RC_mult<int=96, int=48, int=48, int=1>(double const *, double2 const *, double2*)"
18.863999,5.775317,100,0.057753,0.056255,0.062527,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
16.996030,5.203428,100,0.052034,0.051103,0.053471,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
14.121952,4.323513,100,0.043235,0.041919,0.044959,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
3.006093,0.920332,100,0.009203,0.008991,0.009439,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
1.935824,0.592663,400,0.001481,0.001376,0.002432,"[CUDA memcpy DtoH]"
1.453552,0.445013,3,0.148337,0.000992,0.301144,"[CUDA memcpy HtoD]"
1.339715,0.410161,100,0.004101,0.003999,0.004319,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
1.316410,0.403026,102,0.003951,0.003647,0.027167,"[CUDA memset]"
1.112719,0.340665,100,0.003406,0.003359,0.003648,"void __reduce_kernel__<unsigned int=128, double>(double*, double*, int)"

==3820== API calls:
No API activities were profiled.
