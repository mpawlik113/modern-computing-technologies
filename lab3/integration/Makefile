# GPU COMPUTE CAPABILITY
GPU_ARCH = -gencode arch=compute_35,code=compute_35 -gencode arch=compute_37,code=compute_37 -gencode arch=compute_52,code=compute_52 -gencode arch=compute_60,code=compute_60


# C COMPILATION
CC         = gcc
CC_FLAGS   = -Wall -Wundef -m64 -march=native -mtune=native -O3 -fopenmp -fPIC
CC_INC     = -I/usr/local/cuda/include -I/usr/local/cuda/samples/common/inc -I.
CC_DEFS    = -DNX=$(NX) -DNY=$(NY) -DNZ=$(NZ)

# C++ COMPILATION
CXX        = g++
CXX_FLAGS  = -Wall -Wundef -m64 -march=native -mtune=native -O3 -fopenmp -fPIC
CXX_INC    = -I/usr/local/cuda/include -I/usr/local/cuda/samples/common/inc -I.
CXX_DEFS   = -DNX=$(NX) -DNY=$(NY) -DNZ=$(NZ)

# CUDA COMPILATION
NVCC       = nvcc
NVCC_FLAGS = -m64 -O3 $(GPU_ARCH) -Xcudafe "--diag_suppress=declared_but_not_referenced --diag_suppress=set_but_not_used" -Xcompiler "-fPIC -Wall -march=native -mtune=native -O3 -fopenmp"
NVCC_INC   = -I/usr/local/cuda/include -I/usr/local/cuda/samples/common/inc -I. -I/home/dteam050/libs/include/cub/cub/
NVCC_DEFS  = -DNX=$(NX) -DNY=$(NY) -DNZ=$(NZ)

# LINKING
LD         = nvcc
LD_FLAGS   = $(GPU_ARCH)
LIBS       = -L./usr/local/cuda/lib64 -lcudart -lcufft -lcublas -lgomp -lpthread -lm


# PROGRAMS
NORM      = norm.x
EXPEC     = expected_val.x
REDUCE    = reduce_test.x

all: $(NORM) $(EXPEC) $(REDUCE)

expec:  $(EXPEC)

reduce: $(REDUCE)

$(NORM):
	$(NVCC) -c test_norm.cu    $(NVCC_FLAGS) $(NVCC_INC) --keep --keep-dir ./test_norm_keep
	$(LD)   -o $@  test_norm.o $(LD_FLAGS) $(LIBS)
	@rm *.o


$(EXPEC):
	$(NVCC) -c expected_val.cu    $(NVCC_FLAGS) $(NVCC_INC) --keep --keep-dir ./expected_val_keep
	$(LD)   -o $@  expected_val.o $(LD_FLAGS) $(LIBS)
	@rm *.o


$(REDUCE):
	$(NVCC) -c reduce_test.cu    $(NVCC_FLAGS) $(NVCC_INC) --keep --keep-dir ./reduce_test_keep
	$(LD)   -o $@  reduce_test.o $(LD_FLAGS) $(LIBS)
	@rm *.o


timing:
	for NX in 48 64 96 128 256; do \
		NY=48; \
		while [ $${NY} -le $${NX} ] ; do \
			echo $$NX $$NY ; \
			$(NVCC) -c expected_val.cu    $(NVCC_FLAGS) $(NVCC_INC) -DNX=$${NX} -DNY=$${NY} -DNZ=$${NY} ; \
			$(LD)   -o expected_val.exe  expected_val.o $(LD_FLAGS) $(LIBS) -DNX=$${NX} -DNY=$${NY} -DNZ=$${NY} $(NVCC_LIBS); \
			nvprof   --normalized-time-unit ms --profile-api-trace none --log-file data/test.$$NX"x"$$NY"x"$$NY.profile --csv ./expected_val.exe; \
			NY=`expr $$NY + 16`; \
		done; \
	done


clean:
	@rm *.exe
