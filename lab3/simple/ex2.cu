#include <cub/cub.cuh>
#include <cub/block/block_load.cuh>
#include <cub/block/block_store.cuh>
#include <cub/block/block_reduce.cuh>
#include <cuda.h>
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

const int N = 1024;
const int BLOCK_THREADS = 128;
const int ITEMS_PER_THREAD = 1;


__global__ void sum(double *indata, double *outdata) {
    typedef cub::WarpReduce<double,4> WarpReduce;
    __shared__ typename WarpReduce::TempStorage temp_storage;
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    if( id < 128 ) {
        outdata[id] = WarpReduce(temp_storage).Sum(indata[id]);
    }
    if ( id == 0 ) printf("WarpReduce result: %e",outdata[id]);
}

//---------------------------------------------------------------------
// Kernels
//---------------------------------------------------------------------
template <
    typename T,
    int                     BLOCK_THREADS,
    int                     ITEMS_PER_THREAD,
    cub::BlockReduceAlgorithm    ALGORITHM>
__global__ void BlockSumKernel(
    T           *d_in,          // Tile of input
    T           *d_out)         // Tile aggregate
{
    // Specialize BlockReduce type for our thread block
    typedef cub::BlockReduce<T, BLOCK_THREADS, ALGORITHM> BlockReduceT;
    // Shared memory
    __shared__ typename BlockReduceT::TempStorage temp_storage;
    
    // Per-thread tile data
    T data[ITEMS_PER_THREAD];
    cub::LoadDirectStriped<BLOCK_THREADS>(threadIdx.x, d_in, data);
    
    // Compute sum
    T aggregate = BlockReduceT(temp_storage).Sum(data);
    
    // Store aggregate
    if (threadIdx.x == 0)
    {
        *d_out = aggregate;
        printf("cuda: %lf\n",aggregate);
    }
}


/*
 * Compile: nvcc -o ex2.x ex2.cu -O3 -I/home/dteam050/libs/include/cub
 */
int main() {
    cudaSetDevice(0);
    vector<double> y(N), sol(N);
    double *dev_y, *dev_sol;
    cudaMalloc((void**)&dev_y,N*sizeof(double));
    cudaMalloc((void**)&dev_sol,N*sizeof(double));
    for( int i = 0; i < N; i++ ) {
        y[i] = (double) i;
    }
    cout << "input: ";
    for( int i = 0; i < N; i++ ) cout << y[i] << " ";
    cout << endl;
    
    cudaMemcpy(&y[0],dev_y,N*sizeof(double),cudaMemcpyHostToDevice);
    //sum<<<1,32>>>(dev_y,dev_sol);
    BlockSumKernel<double,BLOCK_THREADS,ITEMS_PER_THREAD,cub::BLOCK_REDUCE_RAKING><<<(N+BLOCK_THREADS-1)/BLOCK_THREADS, BLOCK_THREADS>>>(dev_y,dev_sol);
    cudaMemcpy(dev_sol,&sol[0],1*sizeof(double),cudaMemcpyDeviceToHost);
    
    cout << "output: ";
    for( int i = 0; i < 1; i++ ) cout << sol[i] << " ";
    cout << endl;
    cout << "Expected " << (N-1)*N/2 << endl;
    
    cudaFree(dev_y);
    cudaFree(dev_sol);
    return 0;
}