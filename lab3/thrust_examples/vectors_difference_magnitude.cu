#include <iostream>
#include <stdlib.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/tuple.h>
#include <thrust/transform_reduce.h>
#include <thrust/iterator/zip_iterator.h>

typedef thrust::device_vector<float> thrust_vector;
typedef thrust::tuple<float, float> thrust_tuple;

struct func
{
  __device__ float operator()(thrust_tuple t) //difsq
  {
     float f = thrust::get<0>(t) - thrust::get<1>(t);
     return f*f;
  }
};

int main()
{
  thrust_vector a(4, 3.f);
  thrust_vector b(4, 1.f);
  float  result = thrust::transform_reduce(thrust::make_zip_iterator(thrust::make_tuple(a.begin(), b.begin())), 
                                           thrust::make_zip_iterator(thrust::make_tuple(a.end(), b.end())), 
                                           func(), 0.0f, thrust::plus<float>());
  std::cout << "result: " << result << std::endl;
  std::cout << "done" << std::endl;
  return 0;
}