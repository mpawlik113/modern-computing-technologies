#include <thrust/transform.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/iterator/counting_iterator.h>

#include <cstdlib>
#include <iostream>


using namespace thrust;

#define N 32

template <typename T>
struct is_zero : public thrust::unary_function<bool,T>
{
  __host__ __device__
  bool operator()(T s_val) { return (!s_val);}
};

template <typename T>
struct is_one : public thrust::unary_function<bool,T>
{
  __host__ __device__
  bool operator()(T s_val) {return (s_val == 1);}
};



int main()
{
    thrust::device_vector<int> output(N);
    
    std::cout << "device vector allocated..." << std::endl;
    
    float stencil[N] = {0};
    stencil[N-1] = 1;
    stencil[1]   = 1;
    stencil[2]   = 1;
    stencil[N-3] = 1;
    stencil[4]   = 1;
    thrust::device_vector<float> tp_stencil = thrust::host_vector<int>(stencil,stencil+N);
    
    std::cout << "stencil allocated..." << std::endl;
    
    device_vector<int>::iterator output_end = copy_if(make_counting_iterator<int>(0), 
        make_counting_iterator<int>(N), 
        tp_stencil.begin(), 
        output.begin(), 
        is_one<float>());
    
    
    std::cout << "counting..." << std::endl;
    
    
    int number_of_ones = output_end - output.begin();
    
    std::cout << "Number of ones: " << number_of_ones << std::endl;
    
    return EXIT_SUCCESS;
}