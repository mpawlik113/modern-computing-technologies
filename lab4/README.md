# Laboratorium 4

Należy stworzyć macierz liczb losowych (o m kolumnach i n wierszach) a następnie zaimplementować obliczanie macierzy kowariancji. Pamiętać by macierz zaalokować jako jeden blok pamięci i przemyśleć kolejnośc ułożenia wierszy i kolumn pod kątem dostępu do danych w tablicy.

Wykorzystać dowolną implementację BLAS i funkcję *syrk, np.: [https://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-syrk](https://docs.nvidia.com/cuda/cublas/index.html#cublas-lt-t-gt-syrk).

Rozważyć różne implementacje, np. OpenBLAS, BLACS, cuBLAS, Magma. Czy istnieją implementacje umożliwiające korzystanie z wielu GPU na raz?